# Diablo 2 Resurrected Remade

## Greetings, stranger
Welcome to Diablo 2 Resurrected Remade - a very short terminal game where you can
defeat the Den of Evil! The perfect warm-up while waiting for the Resurrected version of D2!

## How to play
Run the Main-class and when the game starts, enter a name and follow the instructions given!

# Project information
In this project I've made an RPG-character creation, with the possibility
to equip armor and weapon onto you character, and level it up. I have tried to 
propagate functions and generalize them so that they can be reused in any child classes.
You can create a mage, ranger, rogue or warrior and equip armor in the head-, body-, or legs-slot or give
them a weapon!
