package no.experis.are.items;

import no.experis.are.attributes.PrimaryAttributes;
import no.experis.are.characters.Warrior;
import no.experis.are.enums.ArmorType;
import no.experis.are.enums.Slot;
import no.experis.are.enums.WeaponType;
import no.experis.are.exceptions.InvalidArmorException;
import no.experis.are.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    private Object InvalidWeaponException;

    @Test
    void EquipWeapon_InvalidEquip_ShouldThrowInvalidWeaponException() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("TestWarrior");
        Weapon highLevelWeapon = new Weapon("Testing Axe", 2, Slot.WEAPON, WeaponType.AXE, 3,3);

        assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipItem(Slot.WEAPON, highLevelWeapon);
        });
    }

    @Test
    void EquipArmor_InvalidEquip_ShouldThrowInvalidArmorException() {
        Warrior warrior = new Warrior("TestWarrior");
        Armor highLevelArmor = new Armor("Testing Armor", 2, Slot.BODY, ArmorType.PLATE, new PrimaryAttributes(0,1,2,1));

        assertThrows(InvalidArmorException.class, () -> {
            warrior.equipItem(Slot.BODY, highLevelArmor);
        });
    }

    @Test
    void EquipWeapon_InvalidWeaponType_ShouldThrowInvalidWeaponException() {
        Warrior warrior = new Warrior("TestWarrior");
        Weapon bow = new Weapon("Testing Bow", 1, Slot.WEAPON, WeaponType.BOW, 1,1);

        assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipItem(Slot.WEAPON, bow);
        });
    }

    @Test
    void EquipArmor_InvalidArmorType_ShouldThrowInvalidArmorException() {
        Warrior warrior = new Warrior("TestWarrior");
        Armor cloth = new Armor("Testing Armor", 1, Slot.BODY, ArmorType.CLOTH, new PrimaryAttributes(1,1,1,1));

        assertThrows(InvalidArmorException.class, () -> {
            warrior.equipItem(Slot.BODY, cloth);
        });
    }

    @Test
    void EquipWeapon_ValidWeapon_ShouldReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("TestWarrior");
        Weapon axe = new Weapon("Testing Axe", 1, Slot.WEAPON, WeaponType.AXE, 3,2);

        warrior.equipItem(Slot.WEAPON, axe);

        assertEquals(axe, warrior.equipment.get(Slot.WEAPON));
    }

    @Test
    void EquipArmor_ValidArmor_ShouldReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("TestWarrior");
        Armor plate = new Armor("Testing Armor", 1, Slot.BODY, ArmorType.PLATE, new PrimaryAttributes(1,1,1,1));

        warrior.equipItem(Slot.BODY, plate);

        assertEquals(plate, warrior.equipment.get(Slot.BODY));
    }

    @Test
    void CalculateDPS_NoWeaponEquipped_ShouldCalculateCorrectly() {
        Warrior warrior = new Warrior("TestWarrior");

        double actual = warrior.getDamage();
        double expected = 1 * (1 + (5 / 100));

        assertEquals(expected, actual);
    }

    @Test
    void CalculateDPS_ValidWeaponEquipped_ShouldCalculateCorrectly() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("TestWarrior");
        Weapon axe = new Weapon("Testing Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);

        warrior.equipItem(Slot.WEAPON, axe);
        double actual = warrior.getDamage();
        double expected = (7 * 1.1)*(1 + (5.0 / 100));

        assertEquals(expected, actual);
    }

    @Test
    void CalculateDPS_ValidWeaponAndArmorEquipped_ShouldCalculateCorrectly() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("TestWarrior");
        Weapon axe = new Weapon("Testing Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        Armor plate = new Armor("Testing Armor", 1, Slot.BODY, ArmorType.PLATE, new PrimaryAttributes(1,1,1,1));

        warrior.equipItem(Slot.WEAPON, axe);
        warrior.equipItem(Slot.BODY, plate);

        double actual = warrior.getDamage();
        double expected = (7 * 1.1) * (1 + ((5.0+1.0) / 100));

        assertEquals(expected, actual);
    }
}