package no.experis.are.characters;

import no.experis.are.attributes.PrimaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void CreateNewMage_ValidCreation_ShouldBeLevel1() {
        Mage mage = new Mage("TestMage");
        int expected = 1;
        int actual = mage.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void LevelUpMage_ValidLevelUp_ShouldBeLevel2() {
        Mage mage = new Mage("TestMage");
        mage.levelUp();
        int expected = 2;
        int actual= mage.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void CreateNewMage_ValidCreation_ShouldHaveCorrectAttributes() {
        Mage mage = new Mage("TestMage");
        PrimaryAttributes expected = new PrimaryAttributes(5,1,1,8);
        PrimaryAttributes actual = mage.getBaseAttributes();

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void CreateNewRanger_ValidCreation_ShouldHaveCorrectAttributes() {
        Ranger ranger = new Ranger("TestRanger");
        PrimaryAttributes expected = new PrimaryAttributes(8,1,7,1);
        PrimaryAttributes actual = ranger.getBaseAttributes();

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void CreatNewRogue_ValidCreation_ShouldHaveCorrectAttributes() {
        Rogue rogue = new Rogue("TestRogue");
        PrimaryAttributes expected = new PrimaryAttributes(8,2,6,1);
        PrimaryAttributes actual = rogue.getBaseAttributes();

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void CreateNewWarrior_ValidCreation_ShouldHaveCorrectAttributes() {
        Warrior warrior = new Warrior("TestWarrior");
        PrimaryAttributes expected = new PrimaryAttributes(10,5,2,1);
        PrimaryAttributes actual = warrior.getBaseAttributes();

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void LevelingUpMage_ValidLevelUp_ShouldHaveIncreasedAttributes() {
        Mage mage = new Mage("TestMage");
        mage.levelUp();
        PrimaryAttributes expected = new PrimaryAttributes(8,2,2,13);
        PrimaryAttributes actual = mage.getBaseAttributes();

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void LevelingUpRanger_ValidLevelUp_ShouldHaveIncreasedAttributes() {
        Ranger ranger = new Ranger("TestRanger");
        ranger.levelUp();
        PrimaryAttributes expected = new PrimaryAttributes(10,2,12,2);
        PrimaryAttributes actual = ranger.getBaseAttributes();

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void LevelingUpRogue_ValidLevelUp_ShouldHaveIncreasedAttributes() {
        Rogue rogue = new Rogue("TestRogue");
        rogue.levelUp();
        PrimaryAttributes expected = new PrimaryAttributes(11,3,10,2);
        PrimaryAttributes actual = rogue.getBaseAttributes();

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void LevelingUpWarrior_ValidLevelUp_ShouldHaveIncreasedAttributes() {
        Warrior warrior = new Warrior("TestWarrior");
        warrior.levelUp();
        PrimaryAttributes expected = new PrimaryAttributes(15,8,4,2);
        PrimaryAttributes actual = warrior.getBaseAttributes();

        assertEquals(expected.toString(), actual.toString());
    }
}