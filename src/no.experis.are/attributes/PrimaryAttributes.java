package no.experis.are.attributes;

public class PrimaryAttributes {

    //FIELDS
    private int vitality;
    private int strength;
    private int dexterity;
    private int intelligence;

    //CONSTRUCTOR
    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    //FUNCTIONS
    public void setAttributePoints(int vitality, int strength, int dexterity, int intelligence) {
        setVitality(vitality);
        setStrength(strength);
        setDexterity(dexterity);
        setIntelligence(intelligence);
    }

    public void updateTotalAttributePoints(int vitality, int strength, int dexterity, int intelligence) {
        resetAttributePoints();
        setVitality(vitality);
        setStrength(strength);
        setDexterity(dexterity);
        setIntelligence(intelligence);
    }

    private void resetAttributePoints() {
        setVitality(0);
        setStrength(0);
        setDexterity(0);
        setIntelligence(0);
    }

    /////////////////////////////////////
    //      GETTERS AND SETTERS        //
    /////////////////////////////////////
    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    //TOSTRING
    @Override
    public String toString() {
        return  "\n   Vitality: " + vitality +
                "\n   Strength: " + strength +
                "\n   Dexterity: " + dexterity +
                "\n   Intelligence: " + intelligence;
    }
}