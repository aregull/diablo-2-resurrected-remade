package no.experis.are.items;

// Cloth, leather, mail, plate

import no.experis.are.attributes.PrimaryAttributes;
import no.experis.are.enums.ArmorType;
import no.experis.are.enums.Slot;

public class Armor extends Item {
    //FIELDS
    private ArmorType armorType;
    private PrimaryAttributes attributes;

    public Armor(String name, int level, Slot slot, ArmorType armorType, PrimaryAttributes primaryAttributes) {
        super(name, level, slot);
        this.armorType = armorType;
        this.attributes = primaryAttributes;
    }

    //GETTERS AND SETTERS
    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(PrimaryAttributes attributes) {
        this.attributes = attributes;
    }
}