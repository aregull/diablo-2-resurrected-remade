package no.experis.are.items;

import no.experis.are.enums.Slot;
import no.experis.are.enums.WeaponType;

public class Weapon extends Item {
    //FIELDS
    private WeaponType weaponType;
    private int damage;
    private double attackSpeed;

    //CONSTRUCTOR
    public Weapon () {
    }

    public Weapon(String name, int level, Slot slot, WeaponType weaponType, int damage, double attackSpeed) {
        super(name, level, slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    //GETTERS AND SETTERS
    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }
}