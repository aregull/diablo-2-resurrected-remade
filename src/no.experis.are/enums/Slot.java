package no.experis.are.enums;

import no.experis.are.items.Item;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON

}
