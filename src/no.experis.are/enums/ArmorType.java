package no.experis.are.enums;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
