package no.experis.are;

import no.experis.are.attributes.PrimaryAttributes;
import no.experis.are.characters.*;
import no.experis.are.characters.Character;
import no.experis.are.enums.ArmorType;
import no.experis.are.enums.Slot;
import no.experis.are.enums.WeaponType;
import no.experis.are.exceptions.InvalidArmorException;
import no.experis.are.exceptions.InvalidInputInRPGSimulation;
import no.experis.are.exceptions.InvalidWeaponException;
import no.experis.are.items.Armor;
import no.experis.are.items.Weapon;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    static List<Character> characters = new ArrayList<>();

    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException, InvalidInputInRPGSimulation, InterruptedException {
        Scanner s = new Scanner(System.in);

        //MAGE ITEMS
        Armor cloth_head = new Armor("Flimsy Cloth Hat", 1, Slot.HEAD, ArmorType.CLOTH, new PrimaryAttributes(2, 1, 1, 3));
        Armor cloth_body = new Armor("Ripped Cloth Sweater", 1, Slot.BODY, ArmorType.CLOTH, new PrimaryAttributes(3, 2, 1, 1));
        Armor cloth_legs = new Armor("Dirty Cloth Pants", 1, Slot.LEGS, ArmorType.CLOTH, new PrimaryAttributes(2, 2, 1, 1));
        Weapon mage_staff = new Weapon("Light Staff", 1, Slot.WEAPON, WeaponType.STAFF, 6, 1.3);
        Weapon mage_wand = new Weapon("Crooked Wand", 1, Slot.WEAPON, WeaponType.WAND, 4, 1.2);

        String name;
        int input;

        System.out.println("""
                ◤ 　                              　　　　◥
                                ⎛⎝(•̀ ‿•)⎠⎞
                                
                                WELCOME TO
                    🔥 DIABLO II RESURRECTED REMADE 🔥
                ◣ 　 　　   　　                          ◢
                                
                For this demo you will be playing as: 🧙 MAGE
                Please enter a name for your character:
                """);

        name = s.nextLine();

        Mage character = new Mage(name);

        System.out.println("""
                --------------------------------------------------------------------------------------------------------------------------------
                💬 The sun is long gone, the wind is whispering words of evil and the rain shows no sign of stopping.
                It's been four full months since you last met a traveller, who warned you to turn around when you still had the chance.
                Arriving on the top of a hill you see a small enclosure with bloody spiked walls and a few dead spiked
                quills laying nearby. Theres a bonfire glowing inside the enclosure and you see a few of the inhabitants guarding the entrance.
                """);
        Thread.sleep(666);
        System.out.println("""
                💬 When you're nearing the enclosure, one of the guards spots you and yells from a distance:
                   'Evil are not welcome here, come any closer and you will face the same destiny as the ones before you!'
                You glance down at the dead spiked quills and quickly reply, saying you're here to fight the recent evil that has arisen.
                The guards let you in, and inside you are greeted by one of the elders, Akara, who lets you use the towns resources if you
                help them cleanse the Den of Evil.
                                
                [TYPE THE NUMBER OF YOUR DESIRED ACTION]:
                1: You ask for more information about the enclosure.
                2: You ask about the other people here.
                3: You go to the shops to get ready for the Den of Evil.
                """);

        input = s.nextInt();

        switch (input) {
            case 1:
                System.out.println("\n💬  You ask Akara for more information about this enclosure, she replies:\n" +
                        "This enclosure is a temporary space for us to survive the " +
                        "great evil that surrounds us now. \nWe've moved several times when our location has " +
                        "been revealed to the hordes of demons who resides in the Monastery.\n\n" +
                        "You thank Akara for the knowledge and visit the shop.");
                break;
            case 2:
                System.out.println("\n💬  You ask Akara for more information about the townspeople, she replies:\n" +
                        "Kashya is the leader of the guarding Rogues. Warriv is a traveller who is waiting here for an\n" +
                        "opportunity to leave for Lut Gholein. Gheed is a sneaky shopkeeper who likes to gamble. Charsi is\n" +
                        "the blacksmith. \nYou thank Akara for the introduction and visit the shop");
                break;
            case 3:
                System.out.println("💬  You thank Akara for the warm welcome and go the shops.");
                break;
            default:
                throw new InvalidInputInRPGSimulation("Error with given input " + input);
        }

        Thread.sleep(3000);
        System.out.println("\n💬  In the shops you meet Charsi, the blacksmith");
        System.out.println("\n'Welcome " + character.getName() + ". Please let me know what you would like to equip.'");

        while (input != 0) {
            System.out.println("\n[TYPE THE NUMBER OF YOUR DESIRED ACTION]:");
            System.out.println("1: 👒 " + cloth_head.getName());
            System.out.println("2: 👕 " + cloth_body.getName());
            System.out.println("3: 👖 " + cloth_legs.getName());
            System.out.println("4: 🦯 " + mage_staff.getName());
            System.out.println("5: 📏 " + mage_wand.getName());
            System.out.println("0: You are ready to cleanse the Den of Evil");


            input = s.nextInt();

            switch (input) {
                case 1:
                    System.out.println("You equip the " + cloth_head.getName());
                    character.equipItem(Slot.HEAD, cloth_head);
                    break;
                case 2:
                    System.out.println("You equip the " + cloth_body.getName());
                    character.equipItem(Slot.BODY, cloth_body);
                    break;
                case 3:
                    System.out.println("You equip the " + cloth_legs.getName());
                    character.equipItem(Slot.LEGS, cloth_legs);
                    break;
                case 4:
                    System.out.println("You equip the " + mage_staff.getName());
                    character.equipItem(Slot.WEAPON, mage_staff);
                    break;
                case 5:
                    System.out.println("You equip the " + mage_wand.getName());
                    character.equipItem(Slot.WEAPON, mage_wand);
                    break;
                case 0:
                    System.out.println("\nYou thank Charsi for her time.");
                    Thread.sleep(1500);
                    break;
                default:
                    throw new InvalidInputInRPGSimulation("Please type one of the given numbers");
            }
        }

        System.out.println("\n[THIS IS THE STATS OF " + character.getName() + " BEFORE ENTERING THE DEN OF EVIL]");
        System.out.println(character);
        System.out.println("\n[TYPE 1 WHEN YOU ARE READY TO ENTER THE DEN OF EVIL]");

        input = s.nextInt();

        System.out.println("You fight the creatures who inhabit the Den of Evil...");
        Thread.sleep(3000);
        System.out.println("There are 5 creatures left alive");
        Thread.sleep(3000);
        System.out.println("4 creatures left alive");
        Thread.sleep(2000);
        System.out.println("3 creatures left alive");
        Thread.sleep(1500);
        System.out.println("2 creatures left alive");
        Thread.sleep(1000);
        System.out.println("1 creatures left alive");
        Thread.sleep(800);
        System.out.println("""
                \n💬 You managed to kill every living creature in the Den of Evil, tired and drenched in blood you find your
                way out of the Den. Near the entrance you spot a dead carver with a shining weapon in its hand. You 
                bring it back for Akara to identify...
                [YOU LEVELLED UP DURING THIS QUEST]
                """);

        character.levelUp();
        character.levelUp();
        Thread.sleep(8000);

        Weapon den_of_evil_weapon = new Weapon("Shaman Staff of Ember", 2, Slot.WEAPON, WeaponType.STAFF, 11, 1.6);
        character.equipItem(Slot.WEAPON, den_of_evil_weapon);
        System.out.println("\n💬 You give Akara the unidentified weapon and she can tell you that this is '" + den_of_evil_weapon.getName() + "'. " +
                "As a thank for doing the quest Akara gives you a Tome of Knowledge and you level up. Abruptly Kashya enters...");

        Thread.sleep(6666);
        System.out.println("""
                                
                THANK YOU FOR PLAYING DIABLO II RESURRECTED REMADE. 
                YOU CAN BUY THE FULL VERSION, OUT SOMETIME... SOON...
                                
                """);

        System.out.println("\n[THIS IS YOUR CHARACTER NOW]");
        System.out.println(character);
    }
}





