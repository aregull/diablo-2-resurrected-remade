package no.experis.are.characters;

import no.experis.are.attributes.PrimaryAttributes;
import no.experis.are.enums.CharacterClasses;
import no.experis.are.enums.Slot;
import no.experis.are.exceptions.InvalidArmorException;
import no.experis.are.exceptions.InvalidWeaponException;
import no.experis.are.items.Armor;
import no.experis.are.items.Item;
import no.experis.are.items.Weapon;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Character {
    //Fields
    private final String name;
    private int level;
    private final PrimaryAttributes baseAttributes;
    private final PrimaryAttributes totalAttributes;
    public HashMap<Slot, Item> equipment;
    private CharacterClasses characterClass;

    //Constructor
    public Character(String name) {
        this.level = 1;
        this.name = name;
        baseAttributes = new PrimaryAttributes(0, 0, 0, 0);
        totalAttributes = new PrimaryAttributes(0, 0, 0, 0);
        equipment = new HashMap<Slot, Item>();
    }

    //FUNCTIONS
    public void levelUp() {
        setLevel(getLevel() + 1);

        switch (characterClass) {
            case MAGE -> setBaseAttributes(3, 1, 1, 5);
            case RANGER -> setBaseAttributes(2, 1, 5, 1);
            case ROGUE -> setBaseAttributes(3, 1, 4, 1);
            case WARRIOR -> setBaseAttributes(5, 3, 2, 1);
            default -> System.out.println("Class not found, no attributes was increased");
        }
    }

    public void setBaseAttributes(int vitality, int strength, int dexterity, int intelligence) {
        baseAttributes.setAttributePoints(
                baseAttributes.getVitality() + vitality,
                baseAttributes.getStrength() + strength,
                baseAttributes.getDexterity() + dexterity,
                baseAttributes.getIntelligence() + intelligence);
    }

    public void setTotalAttributes() {
        AtomicInteger vitality = new AtomicInteger();
        AtomicInteger strength = new AtomicInteger();
        AtomicInteger dexterity = new AtomicInteger();
        AtomicInteger intelligence = new AtomicInteger();

        equipment.forEach(((slot, item) -> {
            if (slot != Slot.WEAPON) {
                vitality.addAndGet(((Armor) item).getAttributes().getVitality());
                strength.addAndGet(((Armor) item).getAttributes().getStrength());
                dexterity.addAndGet(((Armor) item).getAttributes().getDexterity());
                intelligence.addAndGet(((Armor) item).getAttributes().getIntelligence());
            }
        }));

        totalAttributes.updateTotalAttributePoints(
                baseAttributes.getVitality() + vitality.get(),
                baseAttributes.getStrength() + strength.get(),
                baseAttributes.getDexterity() + dexterity.get(),
                baseAttributes.getIntelligence() + intelligence.get()
        );
    }

    public int getHealth() {
        return baseAttributes.getVitality() * 10;
    }

    public double getDamage() {
        Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
        double damage = 0;
        double percentage;

        if (weapon != null) {
            damage = weapon.getDamage() * weapon.getAttackSpeed();
        }

        switch (characterClass) {
            case MAGE -> {
                percentage = totalAttributes.getIntelligence();
                return damage != 0 ? damage * (1 + (percentage / 100)) : 1.0;
            }
            case RANGER, ROGUE -> {
                percentage = totalAttributes.getDexterity();
                return damage != 0 ? damage * (1 + (percentage / 100)) : 1.0;
            }
            case WARRIOR -> {
                percentage = totalAttributes.getStrength();
                return damage != 0 ? damage * (1 + (percentage / 100)) : 1.0;
            }
            default -> System.out.println("Class not found, no attributes was increased");
        }
        return 1.0;
    }

    protected abstract void equipItem(Slot slot, Item item) throws InvalidWeaponException, InvalidArmorException;

    //GETTERS AND SETTERS
    public void setCharacterClass(CharacterClasses characterClass) {
        this.characterClass = characterClass;
    }

    public PrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }

    public PrimaryAttributes getTotalAttributes() {
        setTotalAttributes();
        return totalAttributes;
    }

    public CharacterClasses getCharacterClass() {
        return characterClass;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    //TOSTRING
    @Override
    public String toString() {
        return "Your character is a " + getCharacterClass() +
                " with the name of " + getName() + "." +
                "\nYou are level " + getLevel() + "." +
                "\nYou have max health of " + getHealth() + " and deal " + getDamage() + " damage" +
                "\nThese are your base attributes: " +
                "\n   Vitality: " + baseAttributes.getVitality() +
                "\n   Strength: " + baseAttributes.getStrength() +
                "\n   Dexterity: " + baseAttributes.getDexterity() +
                "\n   Intelligence: " + baseAttributes.getIntelligence() +
                "\nThese are your total attribute points with equipment: " +
                getTotalAttributes()
                ;
    }
}