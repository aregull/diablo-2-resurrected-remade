package no.experis.are.characters;

import no.experis.are.enums.ArmorType;
import no.experis.are.enums.CharacterClasses;
import no.experis.are.enums.Slot;
import no.experis.are.enums.WeaponType;
import no.experis.are.exceptions.InvalidArmorException;
import no.experis.are.exceptions.InvalidWeaponException;
import no.experis.are.items.Armor;
import no.experis.are.items.Item;
import no.experis.are.items.Weapon;

import static no.experis.are.enums.ArmorType.*;
import static no.experis.are.enums.WeaponType.*;


//Staff, wand - weapon
// Cloth - armor
//Primary attribute: Intelligence - increase damage by 1%;

public class Rogue extends Character {

    public Rogue(String name) {
        super(name);
        setCharacterClass(CharacterClasses.ROGUE);
        setBaseAttributes(8,2,6,1);
        setTotalAttributes();

    }

    @Override
    public void equipItem(Slot slot, Item item) throws InvalidWeaponException, InvalidArmorException {
        if (slot.equals(Slot.WEAPON)) {
            equipWeapon((Weapon) item);
        } else {
            equipArmor(slot, (Armor) item);
        }
    }

    private void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        int weaponLevel = weapon.getLevel();
        WeaponType weaponType = weapon.getWeaponType();

        if (!(weaponType.equals(DAGGER) || weaponType.equals(SWORD))) {
            throw new InvalidWeaponException("Weapon type " + weaponType + " is not applicable to class Rogue. " +
                    "Only weapon of type DAGGER or SWORD can be equipped");
        } else if (getLevel() < weaponLevel) {
            throw new InvalidWeaponException("Required level for weapon: " + weapon.getName() +  " is " + weaponLevel + ". Your level is " +
                    getLevel());
        }

        equipment.put(Slot.WEAPON, weapon);
    }

    private void equipArmor(Slot slot, Armor armor) throws InvalidArmorException {
        int armorLevel = armor.getLevel();
        ArmorType armorType = armor.getArmorType();

        if(!(armorType.equals(LEATHER) || armorType.equals(MAIL))) {
            throw new InvalidArmorException("Armor of type " + armorType + " is not applicable to class Mage. " +
                    "Only armor of type LEATHER or MAIL can be equipped");
        } else if (getLevel() < armorLevel) {
            throw new InvalidArmorException("Required level for armor: " + armor.getName() + " is " + armorLevel + ". Your level is " +
                    getLevel());
        }

        equipment.put(slot, armor);
        setTotalAttributes();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}